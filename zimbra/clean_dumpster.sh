#!/bin/bash

# Below define the number of days/months/years to delete dumpster emails. Current is 30d (30 days).
AGE=30d

for account in `zmprov -l gaa`;
do
echo "$account: searching dumpster emails older than $AGE"
zmmailbox -z -m $account s --dumpster -l 1000 --types message before:-$AGE | sed '/^$/d' | awk 'FNR>3 {print "dumpsterDeleteItem " $2}' > /tmp/$account.$$
if [ -f /tmp/$account.$$ ] & [ -s /tmp/$account.$$ ] ; then
COUNT=`wc -l /tmp/$account.$$ | awk '{print $1}'`
echo "$account: deleting $COUNT emails from dumpster"
zmmailbox -z -m $account -A < /tmp/$account.$$;
else
echo "$account: no emails found"
fi
rm -f /tmp/$account_msgs.$$
done