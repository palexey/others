# my others notes
Всяко разно
## bash command

###
   **find**
```bash
find . -type f -exec chmod 644 {} +
find . -type f |xargs chmod 644
find . -name "palexey_backup_test" -type d -exec tar zcvf /tmp/backup.tgz {} +
```
###
   **awk**
```code
$ ls -l | awk '{ print $3,$4 }'
ricardo users
ricardo users
root root
```
###
   **grep**
```bash
grep -v '^ *#\|^ *$'

grep -v '^\s*$\|^\s*\#'
```
###
   **clean dns cache**
```bash
sudo systemd-resolve --flush-caches
sudo /etc/init.d/dns-clean start
```
###
   **Install openvpn**
```bash
wget https://git.io/wireguard -O wireguard-install.sh && sudo bash wireguard-install.sh
wget https://git.io/vpn -O openvpn-install.sh && sudo bash openvpn-install.sh
```
###
   **Windows Time**
```cmd
w32tm /query /configuration
W32tm /query /status /verbose
w32tm /resync /rediscover
```
### 
   **apt util**
```bash
dpkg --remove --force-remove-reinstreq name
apt-get --reinstall install name
apt-get dist-upgrade
```
###
  **disable apt news**
  ```bash
  pro config set apt_news=false
  ```

###
  **get current ext ip**
  ```code
  curl ifconfig.me
  curl -4/-6 icanhazip.com
  curl ipinfo.io/ip
  curl api.ipify.org
  curl checkip.dyndns.org
  dig +short myip.opendns.com @resolver1.opendns.com
  host myip.opendns.com resolver1.opendns.com
  curl ident.me
  curl bot.whatismyipaddress.com
  curl ipecho.net/plain
  ```
###
  **Resize vlm disk**
  ```bash
  echo "- - -" | tee /sys/class/scsi_host/host*/scan | for host in /sys/class/scsi_host/*; do echo "- - -" | sudo tee $host/scan; ls /dev/sd* ; done

  echo 1> /sys/class/block/sda/device/rescan
  cfdisk
  pvresize /dev/sda3
  lvextend -l +100%FREE /dev/mapper/ubuntu--vg-ubuntu--lv
  resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv
  ```
### 
  **Rename volume group**
  ```bash 
  vgdisplay
  lvm vgrename QRBRe4-CGyn-RJP3-wIi2-YQo5-lvty-0yKodf zimbra-lv
  vgchange -ay zimbra-lv
   ```
### 
   **Find package**
   ```bash
   yum provides Prog_Name
   ```
### 
   **UNIX Time**
   ```bash
   timedatectl set-ntp yes
   hwclock --utc --systohc
   ```   
###
   **Get process and port**
   ```powershell
   Get-NetUDPEndpoint | Where {$_.LocalPort -eq "68"} | select LocalAddress,LocalPort,@{Name="Process";Expression={(Get-Process -Id $_.OwningProcess).ProcessName}}
   Get-Process -Id (Get-NetUDPEndpoint -LocalPort YourPortNumberHere).OwningProcess
   Get-Process -Id (Get-NetTCPConnection -LocalPort YourPortNumberHere).OwningProcess
   ```  
###
   **Disable "Started slice"**
   ```bash
   # loginctl enable-linger testuser
   # mkdir /etc/systemd/system/user@1000.service.d
   # cat > /etc/systemd/system/user@1000.service.d/logging.conf <<EOF
     [Service]
     LogLevelMax=notice
     EOF
   #sed -i -e 's/#LogLevel=info/LogLevel=notice/' /etc/systemd/user.conf
   ```
###
  **Sample command ip**
  ```code
  # ip link set dev {DEVICE} {up|down}
  # ip a {add // del} {ipv6_addr_OR_ipv4_addr} dev {interface}
  # ip addr add brd {ADDDRESS-HERE} dev {interface}
  # ip link set txqueuelen {NUMBER} dev {DEVICE}
  # ip link set mtu {NUMBER} dev {DEVICE}
  # ip neigh add {IP-HERE} lladdr {MAC/LLADDRESS} dev {DEVICE} nud {STATE}

```  
###
  **Debug rsyslog**
  ```bash
  rsyslogd -N 1 -d 2> /tmp/rsyslog.stderr.$$  | egrep "cnf:|ssigned|filter|ACTION|PRIFILT" | egrep -v 'cnf:global:script|END' | more
```  
###
  **Firewall sample**
  ```bash
  firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source ipset="my_ip" port port=9090 protocol=tcp accept'
  firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source ipset="my_ip" forward-port port=13389 protocol=tcp to-port=3389 to-addr=192.168.122.85'
  ```
###
  **Bash prompt**
  ```env
  export PS1="\[\e[31m\]\u\[\e[m\]@\[\e[32m\]\H\[\e[m\]\[\e[32m\]:\[\e[m\]\[\e[34m\]\w\[\e[m\] \[\e[36m\]#\[\e[m\]  "
  ```
###  
  **Generation password**
  ```bash
  tr -cd '[:alnum:]' < /dev/urandom| fold -w "${LENGTH}" |head -n 1 |tr -d '\n' | base64 --wrap 0; echo
  ```
  ```bash
  ansible all -i localhost, -m debug -a "msg={{ 'нескажу' | password_hash('sha512') }}"
  ```
  ```bash
  head -c32 /dev/urandom | base64
  ```

###
  **move all dir and files to new place**
  ```bash
  rsync -avh --remove-source-files --info=progress2 --size-only
  ```
###
  **install jq**
  ```bash
wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -O /usr/bin/jq
chmod +x /usr/bin/jq
  ```

###
  **clone servers**
  ```bash
  sudo rsync -vPa -e 'ssh -o StrictHostKeyChecking=no' --exclude-from=/root/exclude-files.txt / REMOTE-IP:/
  ```
###
  **fix broken pip3**
  ```bash
  apt purge python3-pip -y
  apt autoremove
  wget https://bootstrap.pypa.io/get-pip.py
  sudo python3 get-pip.py
  pip install pyopenssl --upgrade
  ```
###
  **for**
  ```bash
  for CEPH in `systemctl list-units --type=service |grep ceph |awk {'print $2'}`; do systemctl stop ${CEPH}; done
  for CEPH in `systemctl list-units --type=service |grep ceph |awk {'print $2'}`; do systemctl disable ${CEPH}; done
  ```
###
  **sed samples**
  ###
  **удаляем пробелы и табуляции с начала каждой строки**
  ```bash
      sed 's/^[ \t]*//'
  ```  
  ***удаляем пробелы и табуляции в конце каждой строки***
  ```text
        sed 's/[ \t]*$//'                    
  ```
  ***удалить пробелы и табуляции как в начале так и в конце строки***
      ```text
        sed 's/^[ \t]*//;s/[ \t]*$//'
      ```
