***bitrix local settings***
```bash
cat /home/bitrix/www/bitrix/.settings.php
```
```bash
cat /home/bitrix/www/bitrix/php_interface/dbconn.php
```
***change acl mysql***
```sql
GRANT SYSTEM_VARIABLES_ADMIN ON *.* TO 'bitrix0'@'localhost';
GRANT SESSION_VARIABLES_ADMIN ON *.* TO 'bitrix0'@'localhost';
```