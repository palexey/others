List command esxi

vim-cmd vmsvc/getallvms |grep <vm name>
vim-cmd vmsvc/power.getstate
vim-cmd vmsvc/get.tasklist VMID
vim-cmd vmsvc/power.{on|off|shutdown} <vmid>

esxcli vm process list
esxcli vm process kill -t={soft|hard|force} -w=WorldID

**backup**
```bash
vim-cmd hostsvc/firmware/sync_config
vim-cmd hostsvc/firmware/backup_config
```
**restore**
```bash
esxcli system maintenanceMode set --enable yes (vim-cmd hostsvc/maintenance_mode_enter)
vim-cmd hostsvc/firmware/restore_config /tmp/configBundle.tgz