## dashboard
**delete**
```bash
$ ceph config rm mgr mgr/dashboard/mgr.<mgr-id>/server_addr
$ ceph config rm mgr mgr/dashboard/<mgr-node-name>/server_addr
$ ceph config rm mgr mgr/dashboard/<mgr-id>/server_addr 
```
**set**
```bash
# ceph config set mgr mgr/dashboard/server_addr $IP
# ceph config set mgr mgr/dashboard/server_port $PORT
# ceph config set mgr mgr/dashboard/ssl_server_port $PORT
```