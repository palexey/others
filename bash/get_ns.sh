#!/bin/bash
if ! [ -z ${1} ]; then
        cp -fr /etc/powerdns/recursor-forward.zones $HOME/recursor-forward.zones.$1
        ns_servers=$(dig @8.8.8.8 $1 NS +short | while read ns; do
            if [[ $ns =~ [a-zA-Z] ]]; then
                ip=$(dig @8.8.8.8 $ns +short)
                echo $ip
            else
                echo $ns
            fi
        done | paste -sd "," -)
        echo $ns_servers
        echo "$1"="$ns_servers" |sed 's/\,\,/\,/g' |sed 's/\,$//g' >> /etc/powerdns/recursor-forward.zones
        sudo /usr/bin/systemctl restart pdns-recursor.service
else
        echo "Domain not aviable"
fi
