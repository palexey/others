#!/bin/bash

# Настройки
DB_USER="your_username"  # Замените на имя пользователя MySQL
DB_PASS="your_password"   # Замените на пароль MySQL
DB_NAME="your_database"   # Замените на имя вашей базы данных
BACKUP_DIR="/path/to/backup"  # Замените на путь к вашей папке для бэкапов
DATE=$(date +%Y%m%d)
WEEKDAY=$(date +%u)
REMOTE_DEST="user@remote_host:/path/to/remote/backup"  # Удаленный адрес

# Создание папки для бэкапов, если она не существует
mkdir -p "$BACKUP_DIR"

# Полный бэкап на 7-й день, инкрементные на остальные
if [ "$WEEKDAY" -eq 7 ]; then
    echo "Проводим полный бэкап базы данных $DB_NAME..."
    mysqldump -u "$DB_USER" -p"$DB_PASS" --databases "$DB_NAME" > "$BACKUP_DIR/full_backup_$DATE.sql"
else
    echo "Проводим инкрементный бэкап базы данных $DB_NAME..."
    mysqldump -u "$DB_USER" -p"$DB_PASS" --databases "$DB_NAME" --single-transaction --flush-logs --quick > "$BACKUP_DIR/incremental_backup_$DATE.sql"
fi

# Архивируем бэкапы, создавая tar.gz
tar -czf "$BACKUP_DIR/backup_$DATE.tar.gz" -C "$BACKUP_DIR" .

# Загрузка архива на удаленный сервер
echo "Загружаем архив на удаленный сервер..."
scp "$BACKUP_DIR/backup_$DATE.tar.gz" "$REMOTE_DEST"

# Удаление локальных бэкапов, если нужно
rm "$BACKUP_DIR/backup_$DATE.tar.gz" # Удалите строку, если хотите сохранить локальные бэкапы

# Удаление бэкапов старше 30 дней
find "$BACKUP_DIR" -type f -mtime +30 -name '*.sql' -exec rm {} \;

echo "Бэкап завершен!"