#!/bin/bash
export IF_INET=enp2s0
export LIMIT=300kbit

tc qdisc add dev ${IF_INET} ingress

tc filter add dev ${IF_INET} protocol ip ingress prio 2 u32 match ip dst 0.0.0.0/0 action police rate ${LIMIT} burst ${LIMIT}
tc filter add dev ${IF_INET} protocol ip ingress prio 2 u32 match ip src 0.0.0.0/0 action police rate ${LIMIT} burst ${LIMIT}
