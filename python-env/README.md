#create env
    ##
     **install nodejs components**
     ```bash
       curl -sL https://deb.nodesource.com/setup_${latest}.x | sudo bash -
       sudo apt install -y nodejs
     ```   
     ##
    **install yarn**
       ```bash
       curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
       echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
       sudo apt update && sudo apt install -y yarn
       ```
    **create env**
    ```bash
    python3 -mvenv ./venv
    source ./venv/bin/activate
    pip install --upgrade pip
    pip install -r requirements.txt
    ```

   