Zabbix notes

{$SERVICE.NAME.NOT_MATCHES}
```code
^(?:RemoteRegistry|MMCSS|gupdate|SysmonLog|clr_optimization_v.+|sppsvc|gpsvc|Pml Driver HPZ12|Net Driver HPZ12|MapsBroker|IntelAudioService|Intel\(R\) TPM Provisioning Service|dbupdate|DoSvc|CDPUserSvc_.+|WpnUserService_.+|OneSyncSvc_.+|WbioSrvc|BITS|tiledatamodelsvc|GISvc|ShellHWDetection|TrustedInstaller|TabletInputService|CDPSvc|wuauserv|edgeupdate|VeeamVssSupport|cbdhsvc_.*|GoogleUpdater*|AdobeARM*)
```

{$NET.IF.IFNAME.NOT_MATCHES}
```code
(^Software Loopback Interface|^NULL[0-9.]*$|^[Ll]o[0-9.]*$|^[Ss]ystem$|^Nu[0-9.]*$|^veth[0-9a-z]+$|docker[0-9]+|br-[a-z0-9]{12}|^vlan[0-9.]+|^asa_mgmt_plane.+|^management.+|^mgmt_plane_int_tap.+|^cplane$|^Internal-Data[0-9]\/[0-9]$|^nlp_int_tap|^STATE$|^Virtual[0-9.]+$|^Fo[0-9]\/[0-9]\/[0-9]\(\)$|^P0[0-9]$|^StackPort[0-9.]|^StackSub-St[0-9]-[0-9]|^Te[0-9]\/[0-9]\/[0-9]|^Twe[0-9]\/[0-9]\/[0-9]|^Vl[0-9.]+|^VLAN-[0-9.]+$|diagnostic|_internal.+|ip6(gre|tnl)[0-9].*$|bridge.*)
```
