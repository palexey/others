#!/bin/bash
 cloneKeyspace()
 {
 	local username="${1}"
 	local start_host="${2}"
 	local hostnames="${3}"
 	local keyspace_source="${4}"
 	local keyspace_destination="${5}"
 	local data_dir="/var/lib/cassandra/ssd/data"​
 	echo "Drop ${keyspace_destination} keyspace"
 	ssh ${username}@${start_host} "cqlsh -u cassandra -p password ${start_host} -e 'DROP KEYSPACE ${keyspace_destination}'"
 	for hostname in $(echo $hostnames | sed "s/,/ /g")
 	do
   	echo "Delete ${keyspace_destination} keyspace directory"
   	ssh ${username}@${hostname} "sudo rm -rf ${data_dir}/${keyspace_destination}"
 	done
 	if ssh ${username}@${start_host} "echo 2>&1"; then
  	ssh ${username}@${start_host} "cqlsh -u cassandra -p password ${start_host} -e 'DESCRIBE KEYSPACE ${keyspace_source}' > ${keyspace_source}.txt"
  	ssh ${username}@${start_host} "sed -i 's/${keyspace_source}/${keyspace_destination}/g' ${keyspace_source}.txt"
  	ssh ${username}@${start_host} "cqlsh -u cassandra -p password ${start_host} -f '${keyspace_source}.txt'"
  	ssh ${username}@${start_host} "rm ${keyspace_source}.txt"     
  	echo "Success on Keyspace Schema clone: "${start_host}
 	else
   	echo "Failed on Keyspace Schema clone: "${start_host}
 	fi
 	
 	for hostname in $(echo $hostnames | sed "s/,/ /g")
 	do
    	if ssh ${username}@${hostname} "echo 2>&1"; then
     	echo "Clear snapshot copy on ${hostname}"
     	ssh ${username}@${hostname} "nodetool clearsnapshot -t copy"
     	echo "Create new snapshot copy on ${hostname}"
     	ssh ${username}@${hostname} "nodetool snapshot -t copy ${keyspace_source}"
     	echo "Success on Cassandra snapshot: "${hostname} ${keyspace_source}
     	
     	sleep 20
     	
  	   ssh ${username}@${hostname} "sudo mv ${data_dir}/${keyspace_source}/metadata-*/snapshots/copy/* ${data_dir}/${keyspace_destination}/metadata-*/"
     	ssh ${username}@${hostname} "sudo mv ${data_dir}/${keyspace_source}/snapshots-*/snapshots/copy/* ${data_dir}/${keyspace_destination}/snapshots-*/"
     	ssh ${username}@${hostname} "sudo mv ${data_dir}/${keyspace_source}/messages-*/snapshots/copy/* ${data_dir}/${keyspace_destination}/messages-*/"
     	ssh ${username}@${hostname} "sudo mv ${data_dir}/${keyspace_source}/tag_scanning-*/snapshots/copy/* ${data_dir}/${keyspace_destination}/tag_scanning-*/"
     	
     	ssh ${username}@${hostname} "nodetool clearsnapshot -t copy"
    	
     	echo "Success on Cassandra mv of snapshot files to destination: "${keyspace_destination}
     	ssh ${username}@${hostname} "nodetool refresh ${keyspace_destination} snapshots"
     	ssh ${username}@${hostname} "nodetool refresh ${keyspace_destination} messages"
     	ssh ${username}@${hostname} "nodetool refresh ${keyspace_destination} metadata"
     	ssh ${username}@${hostname} "nodetool refresh ${keyspace_destination} tag_scanning"
     	
     	echo "Success on Cassandra nodetool refresh on destination keyspace: "${keyspace_destination}
    	else
     	echo "Failed on Cassandra snapshot: "${hostname}
    	fi
 	done
 	
 	if ssh ${username}@${start_host} "echo 2>&1"; then
  	ssh ${username}@${start_host} "nodetool repair -full ${keyspace_destination}"
  	echo "Success on Keyspace repair: "${start_host} ${keyspace_destination}
 	else
  	echo "Failed on Keyspace repair : "${start_host} ${keyspace_destination}
 	fi
 	
 	echo "Script processing completed!"
 }
 ​
 cloneKeyspace "${1}" "${2}" "${3}" "${4}" "${5}"
