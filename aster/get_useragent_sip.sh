for peer in `asterisk -rx 'sip show peers' |awk '/^3/' |awk '{print $1}' |cut -d "/" -f1` ;
        do
                asterisk -rx "sip show peer $peer" |grep -e Callerid -e Useragent -e "Addr->IP"
done